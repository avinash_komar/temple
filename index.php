<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Veerebhadra Devastana Honnagadde</title>
<link rel="icon" href="img/icon.png" />

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/portfolio-item.css" rel="stylesheet">

</head>

<body>
	<div class="container-fluid">
		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top container"
			role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">ಶ್ರೀ ವೀರಭದ್ರಾಯ ನಮಃ</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active" data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1"><a data-toggle="tab"
							href="#home"> <span class="glyphicon glyphicon-file"></span>
								ದೇವಸ್ಥಾನದ ಬಗ್ಗೆ</a>
						</li>
						<li data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1"><a data-toggle="tab"
							href="#trust"> <span class="glyphicon glyphicon-user"></span>
								ನಮ್ಮ ಸಮೀತಿ</a>
						</li data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<li data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1"><a data-toggle="tab"
							href="#gallery"> <span class="glyphicon glyphicon-film"></span>
								ಪಕ್ಷಿ ನೋಟ</a>
						</li>
						<li data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1"><a data-toggle="tab"
							href="#contactus"><span class="glyphicon glyphicon-phone-alt"></span>
								ನಮ್ಮನ್ನು ಸಂಪರ್ಕಿಸಿ</a>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>

		<!-- Page Content -->
		<div class="container cbody">

			<div class="tab-content">

				<div id="home" class="tab-pane fade in active">
					<!-- Portfolio Item Heading -->
					<div class="row well page-header">

						<center>
							<img class="img-responsive" src="img/icon.png" alt="" width="150"
								height="200">
						</center>


						<h1>ಶ್ರೀ ಸಾಂಬಸದಾಶಿವ ಸಹಿತ ವೀರಭದ್ರ ದೇವಸ್ಥಾನ ಹೊನ್ನಗದ್ದೆ</h1>
						<h3>ಅಂಚೆ: ವಜ್ರಳ್ಳಿ ತಾ||ಯಲ್ಲಾಪುರ (ಉ.ಕ) ೫೮೧೩೩೭</h3>

					</div>
					<!-- /.row -->

					<!-- Casrol Item Row -->
					<hr />
					<div id="mySlide" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#mySlide" data-slide-to="0" class="active"></li>
							<li data-target="#mySlide" data-slide-to="1"></li>
							<li data-target="#mySlide" data-slide-to="2"></li>
							<li data-target="#mySlide" data-slide-to="3"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">

							<div class="item active">
								<center><img src="img/page1.jpg" style="width: 60%;"></center>
							</div>

							<div class="item">
								<center><img src="img/page2.jpg" style="width: 60%;"></center>
							</div>

							<div class="item">
								<center><img src="img/page3.jpg" style="width: 60%;"></center>
							</div>

							<div class="item">
								<center><img src="img/page4.jpg" style="width: 60%;"></center>
							</div>

						</div>

						<!-- Left and right controls -->
						<a class="left carousel-control" href="#mySlide"
							data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span>
							<span class="sr-only">Previous</span> </a> <a
							class="right carousel-control" href="#mySlide"
							data-slide="next"> <span
							class="glyphicon glyphicon-chevron-right"></span> <span
							class="sr-only">Next</span> </a>
					</div>
					<hr />
					<!-- Portfolio Item Row -->
					<div class="row">

						<div class="col-md-4">
							<center>
								<img class="img-rounded img-responsive" src="img/pic1.jpg"
									alt="" width="350">
							</center>
						</div>

						<div class="col-md-8">
							<h3>ದೇವಸ್ಥಾನದ ಇತಿಹಾಸ</h3>
							<p class="history">ಶ್ರೀ ಸಾಂಬಸದಾಶಿವ ಸಹಿತ ವೀರಭದ್ರ ದೇವಸ್ಥಾನ
								ಹೊನ್ನಗದ್ದೆ ಇದು ಅತ್ಯಂತ ಪುರಾತನ ದೇವಸ್ಥಾನವಾಗಿದ್ದು, ಸುಮಾರು ೧೦೦೦
								ವರ್ಷಕ್ಕಿಂತಲೂ ಪ್ರಾಚೀನ ದೇವಸ್ಥಾನ ಇರಬಹುದು ಎಂದು ಸುತ್ತಮುತ್ತಲಿನ ಎಲ್ಲರ
								ಕಲ್ಪನೆಯಾಗಿದೆ. ದೇವಸ್ಥಾನದ ಗರ್ಭಗುಡಿಯಲ್ಲಿನ ವಿಗ್ರಹವು ಅತ್ಯಂತ ಸುಂದರ
								ಶಿಲ್ಪಕಲೆಯಿಂದ ಕೂಡಿದ್ದು, ನಾಲ್ಕು ಅಡಿ ಎತ್ತರವಾಗಿದೆ. ಶಿವನನ್ನೇ ತನ್ನ
								ತಲೆಯಮೇಲೆ ಧರಿಸಿರುವ ಈ ದೇವರು ಸುತ್ತಮುತ್ತಲಿನ ಗ್ರಾಮ, ತಾಲೂಕು,
								ಜಿಲ್ಲೆಗಳಲ್ಲದೇ ಹೂರ ರಾಜ್ಯಗಳಿಂದಲೂ ಭಕ್ತ ಸಮೂಹವನ್ನು ಹೊಂದಿದೆ. ಶ್ರೀ
								ಕ್ಷೇತ್ರವು ಹೇಳಿಕೆ, ಹರಕೆಗಳು ನಡೆಯುವ ಪುಣ್ಯ ಸ್ಥಳವಾಗಿದ್ದು, ಶ್ರೀ
								ವೀರಭದ್ರ ದೇವರ ಶಕ್ತಿ ಸದಾ ಜಾಗೃತವಾಗಿರುತ್ತದೆ.</p>

						</div>

					</div>
					<!-- /.row -->

					<!-- Related Projects Row -->
					<hr />
					<div class="row">

						<h3>ಪೂಜಾ ಸಮಯ</h3>
						<div class="col-sm-6">
							<table class="table table-hover">
								<thead>
									<tr class="info">
										<th>ಸಾಮಾನ್ಯ ದಿವಸ</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>ಬೆಳಿಗ್ಗೆ 7.30 ರಿಂದ 9.00</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-sm-6">
							<table class="table table-hover">
								<thead>
									<tr class="danger">
										<th>ಸೋಮವಾರ, ಸಂಕಷ್ಠಿ ಮತ್ತು ವಿಷೇಶ ದಿವಸ (ಹಬ್ಬದ ದಿನಗಳು)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>ಮಧ್ಯಾಹ್ನ 12.00 ರಿಂದ 1.30</td>
									</tr>
								</tbody>
							</table>
						</div>


					</div>
					<br />
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>ಅರ್ಚಕರು</h4>
						</div>
						<div class="panel-body">
							<h4>ಶ್ರೀ ರಾಮಚಂದ್ರ ಭಟ್ಟ ದುಂಡಿ</h4>
							<h4>
								<span class="glyphicon glyphicon-earphone"> ದೂರವಾಣಿ ಸಂಖ್ಯೆ :
									8277270851 
							
							</h4>
						</div>
					</div>
					<br />
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>
								<span class="glyphicon glyphicon-map-marker"></span>&nbsp; ಸ್ಥಳ
								ಸೂಚಿ
							</h4>
						</div>
						<div class="panel-body">
							<div class="clip">
								<script type="text/javascript"
									src="http://maps.google.com/maps/api/js?key=AIzaSyCsfHrTdBJlUDhuMEFONeJ7pQJ70Ry5mkI&sensor=true"></script>
								<div style="overflow: hidden; height: 500px; width: 100%;">
									<div id="gmap_canvas" style="height: 500px; width: 100%;">
										<style>
#gmap_canvas img {
	max-width: none !important;
	background: none !important
}
</style>
									</div>
								</div>
								<script type="text/javascript"> 
                             function init_map(){
                              var myOptions = {
                                zoom:15,center:new google.maps.LatLng(14.873011233883425,74.5871357397034),
                                mapTypeId: google.maps.MapTypeId.ROADMAP};
                                map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
                                marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(14.873011233883425, 74.5871357397034)
                                });
                                infowindow = new google.maps.InfoWindow(
                                {content:"<b>ಶ್ರೀ ವೀರಭದ್ರ ದೇವಸ್ಥಾನ</b><br/> ಹೊನ್ನಗದ್ದೆ<br/>" });
                                google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);
                                });
                                infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);
                            </script>
							</div>
						</div>
					</div>
					<!-- /.row -->

				</div>
				<!--end of home tab -->


				<div id="trust" class="tab-pane fade">
					<?php include 'includings/samiti.php'; ?>
				</div>

				<div id="gallery" class="tab-pane fade">
					<?php include 'includings/gallery.php'; ?>
				</div>

				<div id="contactus" class="tab-pane fade">
					<?php include 'includings/contactus.php'; ?>
				</div>

			</div>


		</div>

	</div>
	<hr />
	<!-- /.container -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>


</body>

</html>
